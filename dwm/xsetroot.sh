#!/bin/bash

while [ True ]
do
	xsetroot -name "$(date) | bat: $(cat /sys/class/power_supply/BAT0/capacity)%"
	sleep 1s
done
